# Street Fighter API server

## Installation
- npm install

### for production
- npm run start 

### for development mode
- npm install -g nodemon
- nodemon ./bin/www

```default server running on http://localhost:3000/```

## Requests

##Server deployed by reference: https://street-fight.herokuapp.com/ via github deploy method

######In order to have access to the requests you need to send a header 'Authorization'  with the value 'admin'

###'get' method /user

To get details all personages in JSON

#####Example: http://localhost:3000/user

###'get' method /user/:id

To get details personage by id in json

#####Example: http://localhost:3000/user/6

###'post' method /user/

Used to create a personage. 

Need to send JSON with the characteristics of the fighter

####Request example:

{
    "_id": "464555",
    "name": "Rick",
    "health": 45,
    "attack": 4,
    "defense": 3,
    "source": "http://www.fightersgeneration.com/np5/char/ssf2hd/ryu-hdstance.gif"
}

Fields health, attack and defense must be of type number;

#####Example: http://localhost:3000/user/

###'put' method /user/:id

Used to update player data by id.

If the personage does not exist, it will be created with id from request

####Request example:

{
    "name": "Rick",
    "health": 45,
    "attack": 4,
    "defense": 3,
    "source": "http://www.fightersgeneration.com/np5/char/ssf2hd/ryu-hdstance.gif"
}

#####Example: http://localhost:3000/user/464555

###'delete' method /user/:id

Used to delete personage by id.

#####Example: http://localhost:3000/user/464555

##Logging

All logs are written to file `appRoot/logs/app.log`