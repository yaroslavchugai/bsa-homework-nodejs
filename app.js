'use strict';

const express = require('express');
const cookieParser = require('cookie-parser');
const morgan = require('morgan');
const appRoot = require('app-root-path');
const fs = require('fs-extra');

const indexRouter = require('./routes/index');
const usersRouter = require('./routes/users');

const accessLogStream = fs.createWriteStream(
  `${appRoot}/logs/app.log`,
  { flags: 'a' }
);

const app = express();

app.use(morgan('combined', { stream: accessLogStream }));

app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());

app.use((req, res, next) => {
  res.header('Access-Control-Allow-Origin', '*');
  res.header('Access-Control-Allow-Methods', 'POST, PUT, GET, DELETE');
  res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type, Accept, Authorization');
  next();
});

app.use('/', indexRouter);
app.use('/user', usersRouter);

module.exports = app;
