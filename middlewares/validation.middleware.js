'use strict';

const typeChecking = (req, res, next) => {
  const { attack, health, defense } = req.body;
  if (
    typeof +attack === 'number' &&
    typeof +health === 'number' &&
    typeof +defense === 'number'
  ) {
    next();
  } else {
    res.status(422).send();
  }
};
module.exports = {
  typeChecking,
};
