'use strict';

const { readJsonSync, writeJsonSync } = require('fs-extra');
const appRoot = require('app-root-path');

const getData = () => {
  const data = readJsonSync(`${appRoot}/db/users.json`);
  if (data) {
    return data;
  } else {
    return false;
  }
}

const saveData = data => {
  try {
    writeJsonSync(`${appRoot}/db/users.json`, data);
    return true;
  } catch (error) {
    return false;
  }
};

module.exports = {
  getData,
  saveData,
};
