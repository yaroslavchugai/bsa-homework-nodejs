'use strict';

const moment = require('moment-timezone');
const appRoot = require('app-root-path');
const { createLogger, format, transports } = require('winston');
const { combine, label, printf } = format;

const myFormat = printf(
  info =>
    `${info.timestamp} [${info.level}]: ${info.label} - ${info.message}`
);

const appendTimestamp = format((info, opts) => {
  if (opts.tz) {
    info.timestamp = moment().tz(opts.tz).format();
  }
  return info;
});

const logger = createLogger({
  level: 'info',
  format: combine(
    label({ label: 'main' }),
    appendTimestamp({ tz: 'Asia/Taipei' }),
    myFormat
  ),
  transports: [
    new transports.Console(),
    new transports.File({
      filename: `${appRoot}/logs/app.log`,
      options: { flags: 'a', mode: 0o666 },
    }),
  ],
});

module.exports = logger;
