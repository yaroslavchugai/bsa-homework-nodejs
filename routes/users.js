'use strict';

const express = require('express');
const router = express.Router();

const {
  getUsers,
  getUserById,
  saveUser,
  updateUser,
  deleteUser,
} = require('../services/user.service');

const {
  isAuthorized,
} = require('../middlewares/auth.middleware');

const {
  typeChecking,
} = require('../middlewares/validation.middleware');

router.get('/',
  isAuthorized,
  (req, res, next) => {
    const result = getUsers();
    if (result) {
      res.send(result);
    } else {
      res.status(400).send(`Some error`);
    }
  });

router.get('/:id',
  isAuthorized,
  (req, res, next) => {
    const result = getUserById(req);
    if (result) {
      res.send(result);
    } else {
      res.status(400).send(`Some error`);
    }
  });

router.post('/',
  isAuthorized,
  typeChecking,
  (req, res, next) => {
    const result = saveUser(req);
    if (result) {
      res.send(result);
    } else {
      res.status(400).send(`Some error`);
    }
  });

router.put('/:id',
  isAuthorized,
  typeChecking,
  (req, res, next) => {
    const result = updateUser(req);
    if (result) {
      res.send(result);
    } else {
      res.status(400).send(`Some error`);
    }
  });

router.delete('/:id',
  isAuthorized,
  (req, res, next) => {
    const result = deleteUser(req);
    if (result) {
      res.send(result);
    } else {
      res.status(400).send(`Some error`);
    }
  });

module.exports = router;
