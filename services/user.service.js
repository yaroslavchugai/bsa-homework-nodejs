'use strict';

const {
  getData,
  saveData,
} = require('../repositories/users.repository');

const userDoesNotExist = req => {
  if (
    req &&
    req.body &&
    req.body._id
  ) {
    const users = getData();
    const { _id } = req.body;
    if (users && _id) {
      for (const user of users) {
        if (user._id === _id) {
          return 'User already exists';
        }
      }
      return true;
    } else {
      return false;
    }
  }
};

const getUsers = () => {
  const users = getData();
  if (users) {
    return users;
  } else {
    return false;
  }
};

const getUserById = req => {
  const { id } = req.params;
  const users = getData();
  if (users && id) {
    for (const user of users) {
      if (user._id === id) {
        return user;
      }
    } return 'User is not found';
  } else {
    return false;
  }
};

const saveUser = req => {
  const { body } = req;
  const users = getData();

  if (userDoesNotExist(req) === true) {
    users.push(body);
    saveData(users);
    if (body) {
      return 'User saved';
    } else {
      return false;
    }
  } else {
    return userDoesNotExist(req);
  }
};

const updateUser = req => {

  const { id } = req.params;
  const { body } = req;

  const users = getData();
  if (id) {
    body._id = id;
    for (const [index, value] of users.entries()) {
      if (value._id === id) {
        for (const key in body) {
          users[index][key] = body[key];
        }
        saveData(users);
        return `${users[index].name} updated`;
      }
    }
    console.log(body);
    saveUser(req);
    return `${body.name} created`;
  } else {
    return 'Some error';
  }
};

const deleteUser = req => {

  const { id } = req.params;
  const users = getData();
  if (id) {
    for (const [index, value] of users.entries()) {
      if (value._id === id) {
        users.splice(index, 1);
        saveData(users);
        return `User deleted`;
      }
    }
    return `User is not found`;
  } else {
    return 'Some error';
  }
};

module.exports = {
  getUsers,
  getUserById,
  saveUser,
  updateUser,
  deleteUser,
};
